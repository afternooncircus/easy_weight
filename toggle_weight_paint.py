import bpy

# This operator is added to the Object menu.

# It does the following:
#	Set active object to weight paint mode
#	Make sure active object is not in wireframe or bounding box display type
#	Set shading mode to a Flat, Single Color, White shading
#	Find first armature via the object's modifiers.
#	Ensure it is visible, select it and set it to pose mode.

# This allows you to start weight painting with a single button press from any state.

# When running the operator again, it should restore everything to how it was before.

def get_armature_of_meshob(obj: bpy.types.Object):
	"""Find and return the armature that deforms this mesh object."""
	for m in obj.modifiers:
		if m.type=='ARMATURE':
			return m.object

def enter_wp(context) -> bool:
	"""Enter weight paint mode, change the necessary settings, and save their
	original states so they can be restored when leaving wp mode."""

	obj = context.object
	wm = context.window_manager

	# Store old display_type setting in a Custom Property on the Object.
	obj['wpt_display_type'] = obj.display_type
	# Ensure display_type is SOLID; weights don't display in WIRE or BOUNDS.
	if obj.display_type not in ['SOLID', 'TEXTURED']:
		obj.display_type = 'SOLID'

	# Store old shading settings in a Custom Property dictionary in the Scene.
	if 'wpt' not in wm:
		wm['wpt'] = {}

	wpt = wm['wpt']
	wpt_as_dict = wpt.to_dict()

	# If we are entering WP mode for the first time or if the last time 
	# the operator was exiting WP mode, then save current shading info.
	if 'last_switch_in' not in wpt_as_dict or wpt_as_dict['last_switch_in']==False:
		wpt['shading_type'] = context.space_data.shading.type
		if context.space_data.shading.type=='SOLID':
			# These properties only exist when the shading type is SOLID.
			wpt['light'] = context.space_data.shading.light
			wpt['color_type'] = context.space_data.shading.color_type
			wpt['single_color'] = context.space_data.shading.single_color
		wpt['active_object'] = obj

	# This flag indicates that the last time this operator ran, we were 
	# switching INTO wp mode.
	wpt['last_switch_in'] = True
	wpt['mode'] = obj.mode

	# Set shading
	if context.space_data.shading.type=='SOLID':
		context.space_data.shading.light = 'FLAT'
		context.space_data.shading.color_type = 'SINGLE'
		context.space_data.shading.single_color = (1,1,1)

	# Enter WP mode.
	bpy.ops.object.mode_set(mode='WEIGHT_PAINT')

	### ENSURING ARMATURE VISIBILITY
	armature = get_armature_of_meshob(obj)
	if not armature:
		return
	# Save all object visibility related info so it can be restored later.
	wpt['arm_enabled'] = armature.hide_viewport
	wpt['arm_hide'] = armature.hide_get()
	wpt['arm_in_front'] = armature.show_in_front
	wpt['arm_coll_assigned'] = False
	armature.hide_viewport = False
	armature.hide_set(False)
	armature.show_in_front = True
	if context.space_data.local_view:
		wpt['arm_local_view'] = armature.local_view_get(context.space_data)
		armature.local_view_set(context.space_data, True)

	# If the armature is still not visible, add it to the scene root collection.
	if not armature.visible_get() and not armature.name in context.scene.collection.objects:
		context.scene.collection.objects.link(armature)
		wpt['arm_coll_assigned'] = True

	if armature.visible_get():
		context.view_layer.objects.active = armature
		bpy.ops.object.mode_set(mode='POSE')
	
	context.view_layer.objects.active = obj
	return armature.visible_get()

def leave_wp(context):
	"""Leave weight paint mode, then find, restore, and delete the data
	that was stored about shading settings in enter_wp()."""

	obj = context.object
	wm = context.window_manager

	# Restore object display type
	if 'wpt_display_type' in obj:
		obj.display_type = obj['wpt_display_type']
		del obj['wpt_display_type']

	if 'wpt' not in wm or 'mode' not in wm['wpt'].to_dict():
		# There is no saved data to restore from, nothing else to do.
		bpy.ops.object.mode_set(mode='OBJECT')
		return {'FINISHED'}

	wpt = wm['wpt']
	wpt_as_dict = wpt.to_dict()

	# Restore mode.
	bpy.ops.object.mode_set(mode=wpt_as_dict['mode'])

	# Reset the stored data
	wm['wpt'] = {}
	# Flag to save that the last time the operator ran we were EXITING wp mode.
	wm['wpt']['last_switch_in'] = False

	# Restore shading options.
	if 'light' in wpt_as_dict:	# If we stored solid view shading settings, restore those settings but don't restore the shading type.
		shading_type = context.space_data.shading.type
		context.space_data.shading.type = wpt_as_dict['shading_type']
		if context.space_data.shading.type=='SOLID':
			context.space_data.shading.light = wpt_as_dict['light']
			context.space_data.shading.color_type = wpt_as_dict['color_type']
			context.space_data.shading.single_color = wpt_as_dict['single_color']
		context.space_data.shading.type = shading_type

	armature = get_armature_of_meshob(obj)
	if not armature:
		return
	# If an armature was un-hidden, hide it again.
	armature.hide_viewport = wpt_as_dict['arm_enabled']
	armature.hide_set(wpt_as_dict['arm_hide'])
	armature.show_in_front = wpt_as_dict['arm_in_front']

	# Restore whether the armature is in local view or not.
	if 'arm_local_view' in wpt_as_dict and context.space_data.local_view:
		armature.local_view_set(context.space_data, wpt_as_dict['arm_local_view'])

	# Remove armature from scene root collection if it was moved there.
	if wpt_as_dict['arm_coll_assigned']:
		context.scene.collection.objects.unlink(armature)

	return
class EASYWEIGHT_OT_toggle_weight_paint(bpy.types.Operator):
	"""Toggle weight paint mode properly with a single operator. """
	bl_idname = "object.weight_paint_toggle"
	bl_label = "Toggle Weight Paint Mode"
	bl_options = {'REGISTER', 'UNDO'}
	
	@classmethod
	def poll(cls, context):
		ob = context.object
		return ob and ob.type=='MESH'

	def draw(self, context):
		self.layout.operator(EASYWEIGHT_OT_toggle_weight_paint.bl_idname)

	def execute(self, context):
		obj = context.object

		if obj.mode != 'WEIGHT_PAINT':
			armature_visible = enter_wp(context)
			if armature_visible == False:
				# This should never happen, but it also doesn't break anything.
				self.report({'WARNING'}, "Could not make Armature visible.")
			return {'FINISHED'}
		else:
			leave_wp(context)
			return {'FINISHED'}

class EASYWEIGHT_PT_Debug(bpy.types.Panel):
	"""For debugging the Toggle Weight Paint operator."""
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'UI'
	bl_category = 'EasyWeight'
	bl_idname = "EASYWEIGHT_PT_Debug"
	bl_label = "Debug Toggle WP"

	@classmethod
	def poll(cls, context):
		return context.preferences.addons[__package__].preferences.debug_mode

	def draw(self, context):
		layout = self.layout
		wm = context.window_manager

		if not 'wpt' in wm: 
			return

		wpt = wm['wpt'].to_dict()

		for key in wpt.keys():
			split = layout.split(factor=0.4)
			value = wpt[key]
			if type(value)==bpy.types.Object:
				value = value.name
			split.label(text=key)
			split.label(text=str(value))

def register():
	from bpy.utils import register_class
	register_class(EASYWEIGHT_OT_toggle_weight_paint)
	register_class(EASYWEIGHT_PT_Debug)

	bpy.types.VIEW3D_MT_paint_weight.append(EASYWEIGHT_OT_toggle_weight_paint.draw)
	bpy.types.VIEW3D_MT_object.append(EASYWEIGHT_OT_toggle_weight_paint.draw)

def unregister():
	from bpy.utils import unregister_class
	unregister_class(EASYWEIGHT_OT_toggle_weight_paint)
	unregister_class(EASYWEIGHT_PT_Debug)

	bpy.types.VIEW3D_MT_paint_weight.remove(EASYWEIGHT_OT_toggle_weight_paint.draw)
	bpy.types.VIEW3D_MT_object.remove(EASYWEIGHT_OT_toggle_weight_paint.draw)